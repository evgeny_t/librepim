#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"
#include <QtQml>
#include <QtCore>
#include "myview.h"

#include <QQmlApplicationEngine>

#include <lpiminmemorytaskprovider.h>
#include <lpimjsontaskprovider.h>

#include "taskviewcontroller.h"

inline QDateTime date(const QString& d = "", int day = 0)
{
    QDate now = QDate::currentDate();
    return d == ""
            ? QDateTime(
                  QDate(now.year(), now.month(), now.day() + day))
            : QDateTime::fromString(
                QString("%1-%2-%3T")
                   .arg(now.year())
                   .arg(now.month(), 2, 10, QChar('0'))
                   .arg(now.day() + day, 2, 10, QChar('0')) + d,
                Qt::ISODate);
}

#if !defined(LPIMTESTS)
int main(int argc, char *argv[])
{
    qDebug() << QTime(24, 0).msecsSinceStartOfDay();
    QGuiApplication app(argc, argv);

//    QDateTime now1 = QDateTime::currentDateTime();
//    qDebug() << now1.toString(Qt::ISODate);
//    QDateTime n2 = QDateTime::fromString("2014-04-07T08:33:41", Qt::ISODate);
//    qDebug() << n2.toString(Qt::ISODate);

    //qmlRegisterType<MyView>("mylib", 1, 0, "MyView");
    //qmlRegisterType<MyView>("mylib", 1, 0, "MyView");
    qmlRegisterType<LPim::LpimTask>("librepim", 1, 0, "Task");

    QSharedPointer<LPim::LpimAbstractTaskProvider> taskProvider =
            QSharedPointer<LPim::LpimAbstractTaskProvider>(
                new LPim::LpimInMemoryTaskProvider
//                new LPim::LpimJsonTaskProvider("test.json")
                );
    LPim::LpimTask *task;

    task = taskProvider->create();
    task->setTitle("call Joe");
    task->setStartTime(date("08:15:00"));
    task->setEndTime(date("08:45:00"));

    task = taskProvider->create();
    task->setTitle("call Alice");
    task->setStartTime(date("07:15:00"));
    task->setEndTime(date("09:45:00"));

task = taskProvider->create();
task->setTitle("call Steve");
task->setStartTime(date("07:15:00"));
task->setEndTime(date("08:00:00"));

    task = taskProvider->create();
    task->setTitle("call Someonewithtoolooongname");
    task->setStartTime(date("09:00:00"));
    task->setEndTime(date("10:15:00"));

    task = taskProvider->create();
    task->setTitle("allday task");
    task->setStartTime(date());
    task->setEndTime(date());

//    task = taskProvider->create();
//    task->setStartTime(date("08:00:00", 1));
//    task->setEndTime(date("08:45:00", 1));

//    task = taskProvider->create();
//    task->setStartTime(date("08:15:00", 2));
//    task->setEndTime(date("09:00:00", 2));

//    task = taskProvider->create();
//    task->setStartTime(date("07:30:00", 3));
//    task->setEndTime(date("11:00:00", 3));

//    task = taskProvider->create();
//    task->setStartTime(date("05:45:00", 4));
//    task->setEndTime(date("12:00:00", 4));

    TaskViewController controller;
    controller.setTaskProvider(taskProvider);

    QQmlApplicationEngine engine;//(QStringLiteral("qml/LibrePIMUI/Main.qml"));
    QQmlContext *context = engine.rootContext();//viewer.rootContext();

    qDebug("context->setContextProperty");
    context->setContextProperty("c", &controller);

    engine.load(QStringLiteral("qml/LibrePIMUI/Main.qml"));
    static_cast<QWindow*>(engine.rootObjects().at(0))->show();


    //QtQuick2ApplicationViewer viewer;



//    viewer.setMainQmlFile(QStringLiteral("qml/LibrePIMUI/Main.qml"));
    //viewer.setSource(QUrl("qrc:/TestQml/qml/main.qml"));


//    viewer.showExpanded();

    return app.exec();
}
#endif
