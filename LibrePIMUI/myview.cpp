#include "myview.h"

#include <QSGGeometryNode>

MyView::MyView(QQuickItem *parent) :
    QQuickItem(parent),
    m_geometry(QSGGeometry::defaultAttributes_Point2D(), 3)
{
    setFlags(ItemHasContents);
    m_material.setColor(Qt::blue);
}

QSGNode *MyView::updatePaintNode(
        QSGNode *n,
        QQuickItem::UpdatePaintNodeData *)
{
    if (!n)
                    n = new QSGNode;

            QSGGeometryNode* geomnode = new QSGGeometryNode();

            QSGGeometry::Point2D* v = m_geometry.vertexDataAsPoint2D();
            const QRectF rect = boundingRect();
            v[0].x = rect.left();
            v[0].y = rect.bottom();
            v[1].x = rect.left() + rect.width()/2;
            v[1].y = rect.top();
            v[2].x = rect.right();
            v[2].y = rect.bottom();
            geomnode->setGeometry(&m_geometry);
            geomnode->setMaterial(&m_material);

            n->appendChildNode(geomnode);
            return n;
//    if (!node)
//        return NULL;
//    qDebug("updatePaintNode");

//    QSGGeometryNode *geomNode = new QSGGeometryNode();

//    QSGGeometry::Point2D* v = m_geometry.vertexDataAsPoint2D();
//    const QRectF rect = boundingRect();
//    v[0].x = rect.left();
//    v[0].y = rect.right();

//    v[1].x = rect.left() + rect.width() / 2.0;
//    v[1].y = rect.top();

//    v[2].x = rect.right();
//    v[2].y = rect.bottom();

//    geomNode->setGeometry(&m_geometry);
//    geomNode->setMaterial(&m_material);
//    node->appendChildNode(geomNode);

//    return geomNode;
}
