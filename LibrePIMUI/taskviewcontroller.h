#ifndef TASKVIEWCONTROLLER_H
#define TASKVIEWCONTROLLER_H

#include <lpimabstracttaskprovider.h>
#include "tasklayoutalgo.h"

#include <QSharedPointer>
#include <QDebug>
#include <QObject>
#include <QDate>

#include <vector>

class TaskViewController : public QObject
{
    Q_OBJECT
public:
    explicit TaskViewController(QObject *parent = 0);

    // here is following requirement for this guy:
    // tasks has to be sorted by startTime along the day.
    Q_INVOKABLE QList<QObject*> fetchTasks(QDate start, QDate end);
    
    Q_INVOKABLE void selectedItem(int index);

    Q_INVOKABLE void resetSelection();

    Q_INVOKABLE bool selected(int index);

    Q_INVOKABLE void addTask(QDateTime begin, QDateTime end);

    Q_INVOKABLE QObject *layoutAlgo();

    void setTaskProvider(QSharedPointer<LPim::LpimAbstractTaskProvider>);

signals:
    void changed();

public slots:
    void taskProviderChanged();

private:
    std::vector<bool> mSelected;
    
    bool mIsSelected;
    QSharedPointer<LPim::LpimAbstractTaskProvider> mTaskProvider;
};

#endif // TASKVIEWCONTROLLER_H
