#include "tasklayoutinfo.h"

TaskLayoutInfo::TaskLayoutInfo(int begin, int end, QObject *parent) :
    QObject(parent),
    begin(begin),
    end(end),
    myWidthDividend(0),
    myWidthDivisor(0),
    myPlaceDividend(0),
    myPlaceDivisor(0)
{
}

int TaskLayoutInfo::widthDividend() const
{
    return myWidthDividend;
}

int TaskLayoutInfo::widthDivisor() const
{
    return myWidthDivisor;
}

int TaskLayoutInfo::placeDividend() const
{
    return myPlaceDividend;
}

int TaskLayoutInfo::placeDivisor() const
{
    return myPlaceDivisor;
}

QObject *TaskLayoutInfo::clone() const
{
    TaskLayoutInfo *result = new TaskLayoutInfo(begin, end);
    result->myPlaceDividend = myPlaceDividend;
    result->myPlaceDivisor = myPlaceDivisor;
    result->myWidthDividend = myWidthDividend;
    result->myWidthDivisor = myWidthDivisor;
    return result;
}

QString TaskLayoutInfo::toString() const
{
    return QString("%1 %2 %3 %4")
            .arg(myPlaceDividend).arg(myPlaceDivisor)
            .arg(myWidthDividend).arg(myWidthDivisor);
}
