import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

RowLayout {
    id: toolbar
    signal changed(int period)
    ToolButton {
        text: "1"
        onClicked: toolbar.changed(1)
    }
    ToolButton {
        text: "3"
        onClicked: toolbar.changed(3)
    }
    ToolButton {
        text: "7"
        onClicked: toolbar.changed(7)
    }
    ToolButton {
        text: "14"
        onClicked: toolbar.changed(14)
    }
}
