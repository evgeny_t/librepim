import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import 'testqml.js' as Code

Rectangle {
    color : "lightgreen"
    id: __placeholderRoot

    property int count/*: 4*/ // TODO: needs a proper name
    property bool highlighted: false
    property int myIndex: index
    property var place: null

    function __forEachSubitem(func) {
        for (var i = 0; i < __placeholderRepeater.count; ++i) {
            func(__placeholderRepeater.itemAt(i));
        }
    }

    function resetSelection() {
        __forEachSubitem(function(x) {
            x.selected = false;
        });
    }

    function select(p0, p1) {
        for (var i = 0; i < __placeholderRepeater.count; ++i) {
            var child = __placeholderRepeater.itemAt(i);
            if (p0.y <= child.y + child.height &&
                    child.y <= p1.y) {
                child.selected = true;
            }
        }
    }

    function range() {
        var first = __placeholderRepeater.count;
        var last = 0;
        for (var i = 0; i < __placeholderRepeater.count; ++i) {
            var child = __placeholderRepeater.itemAt(i);
            if (child.selected) {
                last = Math.max(i, last);
                first = Math.min(i, first);
            }
        }

        if (first <= last) {
            return { first: first * 60.0 / count, last: (last + 1) * 60.0 / count };
        }
        else {
            return null;
        }
    }

    function selectAll() {
        __forEachSubitem(function(x) {
            x.selected = true;
        });
    }

    function getSubplaceHeight() {
        return (__placeholderRoot.height - __layout.spacing * __placeholderRepeater.model) /
                __placeholderRepeater.model;
    }

    function minutesToHeight(min) {
        return Math.floor(min / (60 / __placeholderRepeater.model)) *
                getSubplaceHeight();
    }

    function heightToMinute(h) {
        return Math.floor(h / getSubplaceHeight()) *
                (60 / __placeholderRepeater.model);
    }

    Column {
        id: __layout
        spacing: 0
        Repeater {
            id: __placeholderRepeater
            model: __placeholderRoot.count
            Rectangle {
                property bool selected: false

                border.color: color//Qt.darker(color)
                height: getSubplaceHeight()
                width: __placeholderRoot.width
                radius: 2
                color: {
                    if (selected) {
                        return "red";
                    }
                    else {
                        return "lightgreen";
                    }
                }

                DropArea {
                    anchors.fill: parent
                    onEntered: {
                    }

                    onPositionChanged: {
                        var p = this.parent.mapToItem(__placeholderRoot.parent, 0, 0);
                        drag.source.y = p.y;
                    }

                    onExited: {
                    }

                    onDropped: {
                        var mapped = __placeholderRoot.mapFromItem(
                                    this, drop.x, drop.y);

                        var newDate = new Date(__taskGrid.columnToDate(place.x));
                        newDate.setHours(place.y);
                        var mins = heightToMinute(mapped.y);
                        newDate.setMinutes(mins);

                        __taskGrid.moveTask(drag.source, newDate);
                    }
                }
            }

            function getItemIndexByCoord(x, y) {
                if (x < 0 || y < 0) {
                    return -1;
                }

                for (var i = 0; i < __placeholderRepeater.count; ++i) {
                    var it = __placeholderRepeater.itemAt(i);
                    if ((it.x <= x) && (x <= it.x + it.width) &&
                            (it.y <= y) && (y <= it.y + it.height)) {
                        return i;
                    }
                }
                return -1;
            }

            function getItemByCoord(x, y) {
                var itemIndex = getItemIndexByCoord(x, y);
                if (itemIndex < 0) {
                    return null;
                }

                return __placeholderRepeater.itemAt(itemIndex);
            }
        }
    }
}
