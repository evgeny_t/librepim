import QtQuick 2.3

// time bar
Grid {
    id: __root
    property alias count: __root.rows
    property int itemHeight
    property int itemWidth
    property int startHour

    columns: 1
    Repeater {
        model: parent.rows
        Rectangle {
            width: itemWidth
            height: itemHeight
            color: "red"
            Text {
                text: new Date(0, 0, 0, startHour + index, 0)
                    .toLocaleTimeString("h:m ap")
            }
        }
    }
}
