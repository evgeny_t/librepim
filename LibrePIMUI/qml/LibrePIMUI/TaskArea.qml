import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import 'testqml.js' as Js
import 'testqml.js' as Code

Item {
    property var controller: null
    property var areaWidth

    property int __freeTasksHeight: 50

    property int __rowHeight: 50
    property int __columnWidth: 100
    property int __timebarWidth: 50

    property int __timebarToTaskgridSpacing: 3

    property int __gridRows: 24
    property int gridColumns: 4

    property int __spacing: 3

    property date firstDay: new Date() // today
    property int __starthour: 0

    Layout.preferredWidth: areaWidth

    id: __root

    focus: true

    // TODO: make private
    function updateHeight() {
        var minimalHeight =
                (height - __scrollView.y - __gridRows * 4) / __gridRows;
        if (minimalHeight > __rowHeight) {
            __rowHeight = minimalHeight;
        }
    }

    function updateWidth() {
        __columnWidth =
                (width - __timebarWidth -
                 (gridColumns) * __spacing - __timebarToTaskgridSpacing) / gridColumns;
    }

    onWidthChanged: {
        updateWidth();
        reloadTasks(); // TODO: relayout task
    }
    onHeightChanged: {
        updateHeight();
        reloadTasks(); // TODO: relayout task
    }

    Keys.onPressed: {
        console.log("Keys.onPressed");
        onKeyPressed(event);
    }

    // it has to be passed from controller
    property int hourParts: 2

    function timeToPos(t) {
        var hours = t.getHours();
        var minutes = t.getMinutes();

    }

    function reloadTasks() {
        __taskGrid.reloadTasks();
    }

    Component.onCompleted: {
        console.log("TaskArea: Component.onCompleted: begin");

        if (controller === undefined) {
            console.error("controller shouldn't be undefined");
            return;
        }

        controller.changed.connect(reloadTasks);
        reloadTasks();

        controller.layoutAlgo().layout([]);

        console.log("TaskArea: Component.onCompleted: end");
    }

    ColumnLayout {
        anchors.fill: parent
        id: __rootColLayout

        // dates
        RowLayout {
            id: __datesbar
            //spacing: 10
            Rectangle { // dummy
                width: __timebarWidth
            }

            Row {
                spacing: __taskGrid.spacing

                Repeater {
                    model: gridColumns
                    Text {
                        width: __columnWidth
                        text: __datesbar.indexToDateString(index)
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }

            function indexToDateString(index) {
                var temp = firstDay;
                temp.setDate(temp.getDate() + index);
                return temp.toLocaleDateString(
                            Qt.locale(), "dd.MM");
            }
        }

        Rectangle {
            clip: true
            anchors.fill: __scrollView
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: __

            ColumnLayout {
                anchors.fill: parent
                RowLayout {
                    spacing: 0
                    Rectangle { // dummy
                        width: __timebarWidth + __spacing
                    }

                    // allday task bar
                    AlldayTaskbar {
                        id: allday
                        columns: __taskGrid.columns
                        columnsSpacing: __taskGrid.spacing
                        spacing: __timebarToTaskgridSpacing
                        itemWidth: __columnWidth
                        itemHeight: __freeTasksHeight
                    }
                }

                ScrollView {
                    id: __scrollView

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    RowLayout {
                        spacing: __timebarToTaskgridSpacing

                        Timebar {
                            spacing: __spacing
                            count: __gridRows
                            itemHeight: __rowHeight
                            itemWidth: __timebarWidth
                            startHour: __starthour
                        }

                        // task grid
                        TaskGrid {
                            id: __taskGrid
                            columns: gridColumns
                            rowHeight: __rowHeight
                            columnWidth: __columnWidth
                            rows: __gridRows
                            spacing: __spacing

                            onColumnsChanged: {
                                __root.updateWidth();
                                __root.reloadTasks();
                            }

                            Keys.onPressed: onKeyPressed(event)
                        }
                    }
                }
            }
        }

        // TODO: fix TaskView's mouse area response for mouse events
        // it seems that we need to move to TaskGrid inside of scrollarea
        MouseArea {
            parent: __
            anchors.fill: parent

            // fixs srollbar response for mouse events;
            // actually, it works when the size of mousearea
            // is equal to the scrolview size
            anchors.rightMargin: 15
            anchors.bottomMargin: 15

            hoverEnabled: true

            onPressed: {
                console.log("TaskArea: onPressed");
                var p = __root.mapToItem(
                            __taskGrid, mouse.x, mouse.y);
                var hitTask = __taskGrid.isThereTask(p.x, p.y);

                if (!hitTask) {
                    __taskGrid.beginSelection(p);
                }

                mouse.accepted = !hitTask;
            }

            onReleased: {
                var p = __root.mapToItem(
                            __taskGrid, mouse.x, mouse.y);
                __taskGrid.endSelection(p);
            }

            onWheel: {
                if (wheel.modifiers & Qt.ControlModifier) {
                    if (wheel.angleDelta.y > 0) {
                        __rowHeight += 10;
                    }
                    else {
                        __rowHeight -= 10;
                    }

                    __root.updateHeight();
                    __root.reloadTasks();
                    wheel.accepted = true;
                }
                else {
                    __root.reloadTasks();
                    wheel.accepted = false;
                }
            }

            onPositionChanged: {
                if (mouse.buttons & Qt.LeftButton) {
                    var p = __root.mapToItem(
                                __taskGrid, mouse.x, mouse.y);
                    __taskGrid.select(p);
                }
                else {
                    // TODO:
                }
            }
        }
    }



    // TODO: keyboard handling
    function onKeyPressed(event) {
        event.accepted = true;

        if (event.key === Qt.Key_Escape) {
            __taskGrid.resetSelection();
            return;
        }

// TODO: move to function
        if (event.key === Qt.Key_Q)
        {
            var range = __taskGrid.getSelectedRange();
            if (range !== null) {
                controller.addTask(range.begin, range.end);
            }
            return;
        }
    }
}
