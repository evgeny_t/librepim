
//function getBlockByCoord(x, y) {
//    console.log("getBlockByCoord", x, y)
//    for (var i = 1; i < repeater.children.length; i++) {
//        var c = repeater.children[i];
//        if (c.x <= x && x <= c.x + c.width
//                && c.y <= y && y <= c.y + c.height) {
//            c.opacity = 0.5;
//        }
//    }
//}

//function updateSelection(co) {
//    //console.log("updateSelection")
//    for (var i = 0; i < repeater.count; i++) {
//        var child = repeater.itemAt(i)
//        //child.color = "lightgreen";
//        child.opacity = 1.0;
//        if (co.selected(i) === true) {
//            child.opacity = .5;
//        }
//    }
//}

function inside(p, first, second) {
    var maxx = Math.max(first.x, second.x);
    var minx = Math.min(first.x, second.x);

    var maxy = Math.max(first.y, second.y);
    var miny = Math.min(first.y, second.y);

    return minx <= p.x && p.x <= maxx &&
            miny <= p.y && p.y <= maxy;
}

function itemCenter(item) {
    return Qt.point(item.x + item.width / 2,
                    item.y + item.height / 2);
}

//function updateSelectedItems(controller, first, second) {
//    for (var i = 0; i < repeater.count; i++) {
//        var child = repeater.itemAt(i);
//        child.opacity = 1.0;
//        if (inside(itemCenter(child), first, second)) {
//            child.opacity = 0.5;
//        }
//    }
//}
