import QtQuick 2.3

// task grid
Grid {
    focus: true
    id: __taskGrid

    property int rowHeight
    property int columnWidth

    property var controller: null

    property int __first: -1
    property int __last: -1
    property bool __isDragging: false

    property point __firstPoint
    property point __lastPoint

    function __getItemCoords(index) {
        var x0 = index % columns;
        var y0 = Math.floor(index / columns);
        return Qt.point(x0, y0);
    }

    Repeater {
        id: __repeater
        model: parent.rows * parent.columns

        Placeholder {
            height: rowHeight
            width: columnWidth
            count: 4
            place: __getItemCoords(index)
        }

        function resetSelection() {
            for (var i = 0; i < __repeater.count; ++i) {
                var child = __repeater.itemAt(i);
                child.resetSelection();
            }
        }

        function getItem(x0, y0) {
            var itemIndex = x0 + y0 * parent.columns;
            var child = __repeater.itemAt(itemIndex);
            return child;
        }

        function isThereTask(x0, y0) {
            for (var i = 0; i < __repeater.children.length; ++i) {
                var item = __repeater.children[i];
                if (item.x <= x0 && x0 <= item.x + item.width &&
                        item.y <= y0 && y0 <= item.y + item.height) {
                    return item;
                }
            }

            return null;
        }
    }

    function isThereTask(x, y) {
        var mapped = __repeater.mapFromItem(__taskGrid, x, y);
        return __repeater.isThereTask(mapped.x, mapped.y);
    }

    function resetSelection() {
        __repeater.resetSelection();
        __first = __last = -1;
    }

    function beginSelection(mouse) {
        var task = __repeater.isThereTask(mouse.x, mouse.y);
        if (task !== null) {
            task.select();
            return;
        }

        __isDragging = false;
        __first = getCurrentIndex(mouse);
        __firstPoint = Qt.point(mouse.x, mouse.y);
    }

    function getSelectedRange() {
        if (__first == -1 || __last == -1) {
            return null;
        }

        var p0 = __taskGrid.__getItemCoords(__first);
        var p1 = __taskGrid.__getItemCoords(__last);

        var item0 = __repeater.getItem(p0.x, p0.y);
        var item1 = __repeater.getItem(p1.x, p1.y);

        if (item0 !== null && item1 !== null) {
            var begin = columnToDate(p0.x);
            var end = columnToDate(p1.x);

            begin.setHours(p0.y);
            end.setHours(p1.y);

// TODO: fit minutes into discrete range
            begin.setMinutes(item0.range().first);
            end.setMinutes(item1.range().last);
            return { begin: begin, end: end };
        }

        return null;
    }

    function select(mouse) {
        __isDragging = true;

        var task = __repeater.isThereTask(mouse.x, mouse.y);
        if (task !== null) {
            return;
        }

        __repeater.resetSelection();
        __last = getCurrentIndex(mouse);
        __lastPoint = Qt.point(mouse.x, mouse.y);

        var first = __first;
        var last = __last;

        if (first >= 0 && last >= 0) {
            var p0 = __taskGrid.__getItemCoords(first);
            var x0 = p0.x;
            var y0 = p0.y;

            var p1 = __taskGrid.__getItemCoords(last);
            var x1 = x0; // p1.x; // <-- uncomment to let it select several columns
            var y1 = p1.y;

            if (x0 > x1) {
                var temp = x0; x0 = x1; x1 = temp;
                temp = y0; y0 = y1; y1 = temp;
            }
            else if (x0 === x1) {
                if (y0 > y1) {
                    temp = y0; y0 = y1; y1 = temp;
                }
            }

            // item: task grid item;
            // x, y: mouse coords in task grid coord system;
            var select = function(item, x, y) {
                if (item !== null) {
                    var mapped0 = __repeater.mapToItem(
                                item, __firstPoint.x, __firstPoint.y);
                    var mapped1 = __repeater.mapToItem(
                                item, __lastPoint.x, __lastPoint.y);
                    item.select(mapped0, mapped1);
                }
            }

            while (x0 !== x1 || y0 !== y1) {
                var child = __repeater.getItem(x0, y0);
                select(child);

                y0++;
                if (y0 >= __taskGrid.rows) {
                    y0 = 0;
                    x0++;
                }
            }
            select(__repeater.getItem(x0, y0));
        }
    }

    function endSelection(mouse) {
        if (!__isDragging) {
            __repeater.resetSelection();
        }
    }

    function getCurrentIndex(mouse) {
        // mouse - mouse coords in grid's coordinate system
        var child = childAt(mouse.x, mouse.y);
        if (child !== null) {
            return child.myIndex;
        }
        return -1;
    }

    function columnToDate(col) {
        var temp = firstDay;
        temp.setMinutes(0);
        temp.setSeconds(0);
        temp.setMilliseconds(0);
        temp.setDate(temp.getDate() + col);
        return temp;
    }

    function pointToDate(x, y) {
        var p = Qt.point(x, y);
        var coords = __taskGrid.__getItemCoords(getCurrentIndex(p));
//        console.log('pointToDate: ', p, ' ', coords);

        var placeholder = __repeater.getItem(coords);
        var result = new Date(columnToDate(coords.x));
        result.setHours(coords.y);

        var mapped = __taskGrid.mapToItem(placeholder, x, y);
        var minutes = placeholder.heightToMinute(mapped.y)
//        console.log('pointToDate: minutes = ', minutes, ' ', mapped.y);
//        console.log('pointToDate: result = ', result);
        return result;
    }

    function dateToIndex(date) {
        // TODO: make it O(1) rather than O(columns)
        for (var i = 0; i < columns; ++i) {
            var temp = firstDay; // TODO: make firstDay property
            temp.setDate(temp.getDate() + i);
            if (temp.getDay() === date.getDay()) {
                return i;
            }
        }
        return -1;
    }

    function moveTask(taskview, newdate) {
        taskview.model.move(newdate);
// TODO: task providers have to emit signals when task are changed
        reloadTasks();
    }

    // TODO: move to js
    function rangesAreCrossed(begin1, end1, begin2, end2) {
        return !(end1 <= begin2 || end2 <= begin1);
    }

    function tasksAreCrossed(task1, task2) {
        return rangesAreCrossed(
                    task1.startTime, task1.endTime,
                    task2.startTime, task2.endTime);
    }

    property var colors: [ "green", "grey", "blue" ]

    function updateTaskLayout(taskInfo, taskView) {
        var task = taskView.model;
        var xx = columnWidth * taskInfo.placeDividend() / taskInfo.placeDivisor();
        var ww = columnWidth * taskInfo.widthDividend() / taskInfo.widthDivisor();

        var t1 = task.startTime;
        var t2 = task.endTime;

        // take first as they are the same,
        // we don't need particular one.
        var placeholder = __repeater.itemAt(0);
        var firstShift =
                placeholder.minutesToHeight(t1.getMinutes());
        var y = t1.getHours() *
                (rowHeight + __spacing) + firstShift;


        var secondShift =
                placeholder.minutesToHeight(t2.getMinutes());
        var height = (t2.getHours() - t1.getHours()) *
                (rowHeight + __spacing) + secondShift - firstShift;

        var dayIndex = dateToIndex(t1);
        var rect = Qt.rect(
                    (columnWidth + __spacing) * dayIndex + xx,
                    y, ww, height);

        var place = __repeater.mapToItem(taskView.parent, rect.x, rect.y)
//        var place = rect;
        taskView.x = place.x;
        taskView.width = rect.width;
        taskView.y = place.y;
        taskView.height = rect.height;
    }

    // need to know task-taskview relation
//    function relayout() {
//        for (var dayIndex = 0; dayIndex < __gridColumns; ++dayIndex) {
//            var begin = new Date(firstDay.getDate() + day);
//            var end = new Date(firstDay.getDate() + day + 1);

//            var tasks = c.fetchTasks(begin, end);
//            var layout = c.layoutAlgo().layout(tasks);
//            for (var i = 0; i < tasks.length; ++i) {
//// TODO: task view object
//                updateTaskLayout(layout[i], tasks[i], TODO);
//            }
//        }
//    }

    property var __taskviews: []

    function destroyTaskViews() {
// TODO: __taskviews is undefined
        if (__taskviews !== undefined)
        for (var i = 0; i < __taskviews.length; ++i) {
            // HACK: without delay it doesn't destroy object
            __taskviews[i].destroy(1);
        }

        __taskviews = [];
    }

    function addTaskView(taskview) {
        __taskviews.push(taskview);
    }

    function reloadTasks() {
        console.log('reloadTasks(): ');
        destroyTaskViews();

//        for (var k = 0; k < __repeater.children.length; ++k) {
//            // HACK: without delay it doesn't destroy object
//            __repeater.children[k].destroy(1);
//        }

        for (var day = 0; day < gridColumns; ++day)
        {
            var begin = firstDay;
            var end = firstDay;
            begin.setDate(begin.getDate() + day);
            end.setDate(end.getDate() + day + 1);
            var tasks = c.fetchTasks(begin, end);
            tasks = tasks.filter(function (x) {
                return x.startTime.getTime() !== x.endTime.getTime();
            });

            var layout = c.layoutAlgo().layout(tasks);

            for (var j = 0; j < tasks.length; ++j) {
                // index of the day (column)
                var smth = tasks[j];
// TODO: __datesbar
                var dayIndex = dateToIndex(tasks[j].startTime);
                if (dayIndex >= 0) {
                    var rectangle = Qt.createComponent("TaskView.qml");
                    if (rectangle.status === Component.Ready) {
                        var o = rectangle.createObject(
                                    __
                                    /*repeater*/
//                                    __root
                                    );
                        if (o === null) {
                            console.error("creating taskview failed");
                            return;
                        }

                        addTaskView(o);

                        //o.parent = visualParent
                        o.model = tasks[j];
                        updateTaskLayout(layout[j], o);
                        o.color = colors[
                                    Math.floor(Math.random() * colors.length)];
                    }
                }
            }
        }
    }
}
