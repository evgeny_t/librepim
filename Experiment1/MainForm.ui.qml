import QtQuick 2.3
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2

Rectangle {
    width: 800
    height: 600

    id: __root
    property int itemsHeight: 100

    focus: true
    Keys.onSpacePressed: {
        if (__mousearea.selection) {
            var newObject = Qt.createQmlObject('import QtQuick 2.3; Rectangle {color: "red"; width: 20; height: 20}',
                __columnsRepeater, "dynamicSnippet1");

            newObject.x = __timebarColumn.width + __mousearea.selectedColumn * 100;
            newObject.y = __row.x + __mousearea.selectionRange[0] * 100 / __mousearea.subitems;
            newObject.width = 100;
            newObject.height =
                    (__mousearea.selectionRange[1] - __mousearea.selectionRange[0] + 1) * 100 / __mousearea.subitems;
        }
    }

    MouseArea {
        id: __mousearea
        hoverEnabled: true
        anchors.fill: parent

        signal reset;

        property int subitems: 3

        property bool selection: false
        property int selectedColumn: -1
        property var selectionRange: [10000, 0]

        onSelectionRangeChanged: {
            if (selectedColumn >= 0) {
                var column = __columnsRepeater.itemAt(selectedColumn);
                for (var i = selectionRange[0]; i <= selectionRange[1]; ++i) {
                    var j = Math.floor(i / subitems);
                    var k = i % subitems;
                    column.children[j].highlight(k);
                }
            }
        }

        onPressed: {
            reset();
            selection = true;
            selectedColumn = -1;
            selectionRange = [10000, 0];
        }

        onPositionChanged: {
            if (mouse.buttons & Qt.LeftButton) {
                var column = null;
                if (selectedColumn >= 0) {
                    column = __columnsRepeater.itemAt(selectedColumn);
                } else {
                    var columnInfo = __columnsRepeater.columnAt(parent, mouse.x, mouse.y);
                    if (!columnInfo) {
                        console.error('columnInfo == null');
                        return;
                    }

                    column = columnInfo.column;
                    selectedColumn = columnInfo.index;
                }

                if (!column) {
                    return;
                }

                for (var j = 0; j < column.children.length; ++j) {
                    var rect = column.children[j];
                    var mapped = parent.mapToItem(rect, mouse.x, mouse.y);
                    if (mapped.y >= 0 && mapped.y <= rect.height) {
                        var k = Math.floor(mapped.y / (rect.height / subitems));
                        k = k + subitems * j;
                        selectionRange = [Math.min(k, selectionRange[0]),
                                          Math.max(k, selectionRange[1])]
                    }
                }
            }
        }

        onWheel: {
            var sign = wheel.angleDelta.y > 0 ? 1 : -1;
            if (wheel.modifiers == Qt.ControlModifier) {
                __root.itemsHeight = Math.max(
                            __root.itemsHeight + sign * 10, 20);
            } else {
                __row.y = __row.y + sign * 10;
            }

            wheel.accepted = wheel.modifiers != 0;
            wheel.accepted = false;
        }
    }

    Row {
        id: __row
        Column {
            id: __timebarColumn;
            width: 105
            height: 24 * __root.itemsHeight

            Repeater {
                model: 24

                Rectangle {
                    width: 100
                    height: __root.itemsHeight
                    color: "#d1e8d0"
                    border.width: 1

                    Text {
                        function update() {
                            var d = new Date();
                            d.setHours(index);
                            d.setMinutes(0);
                            return ('0' + d.getHours().toString()).slice(-2) + ':' +
                                    ('0' + d.getMinutes().toString()).slice(-2);
                        }


                        text: qsTr(update(index))
                        font.family: "Arial"
                        font.pointSize: 12
                        anchors.right: parent.right
                        anchors.rightMargin: 5
                    }

                    Canvas {
                        anchors.fill: parent
                        onPaint: {
                            var ctx = getContext("2d");

                            ctx.strokeStyle = "#79e297";

                            for (var i = 0; i < __mousearea.subitems; ++i) {
                                ctx.beginPath();
                                ctx.moveTo(2,
                                           (i + 1) * this.height / __mousearea.subitems);
                                ctx.lineTo(this.width - 2,
                                           (i + 1) * this.height / __mousearea.subitems);
                                ctx.lineWidth = 1;
                                ctx.stroke();
                            }
                        }
                    }
                }
            }
        }

        Repeater {
            model: 7
            id: __columnsRepeater

            // (x,y) is a cursor coordinates related to item
            function columnAt(item, x, y) {
                for (var i = 0; i < this.count; ++i) {
                    var column = this.itemAt(i);
                    var mapped = item.mapToItem(column, x, y)
                    if (mapped.x >= 0 && mapped.x <= column.width &&
                            mapped.y >= 0 && mapped.y <= column.height) {
                        return { column: column, index: i };
                    }
                }

                return null;
            }

            Column {
                width: 100
                height: 24 * __root.itemsHeight

                Repeater {
                    id: __rowsRepeater
                    model: 24

                    Rectangle {
                        anchors.left: __rowsRepeater.parent.left
                        anchors.right: __rowsRepeater.parent.right
                        height: __root.itemsHeight
                        border.color: "#1db522"
                        border.width: 1
                        color: "#b7f8b5"

                        property var selectionRange: [10000, 0]

                        Component.onCompleted: {
                            __mousearea.reset.connect(this.reset);
                        }

                        onSelectionRangeChanged: {
                            children[0].requestPaint();
                        }

                        function highlight(i) {
                            selectionRange = [Math.min(i, selectionRange[0]),
                                              Math.max(i, selectionRange[1])];
                        }

                        function reset() {
                            selectionRange = [10000, 0];
                        }

                        Canvas {
                            anchors.margins: 1
                            anchors.fill: parent
                            onPaint: {
                                var ctx = getContext("2d");

                                ctx.fillStyle = parent.color;
                                ctx.fillRect(0, 0, parent.width, parent.height);

                                ctx.strokeStyle = "#79e297";

                                for (var i = 0; i < __mousearea.subitems; ++i) {
                                    ctx.beginPath();
                                    ctx.moveTo(2,
                                               (i + 1) * this.height / __mousearea.subitems);
                                    ctx.lineTo(this.width - 2,
                                               (i + 1) * this.height / __mousearea.subitems);
                                    ctx.lineWidth = 1;
                                    ctx.stroke();
                                }

                                if (selectionRange) {
                                    for (var i = selectionRange[0]; i <= selectionRange[1]; ++i) {
                                        ctx.fillStyle = "yellow";
                                        ctx.fillRect(0, i * this.height / __mousearea.subitems,
                                                    this.width,
                                                    this.height / __mousearea.subitems);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
