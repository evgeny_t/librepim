#include "lpimjsontaskprovider.h"

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

LPIM_BEGIN_NAMESPACE

LpimJsonTaskProvider::LpimJsonTaskProvider(
        const QString & filename,
        QObject *parent) :
            LpimAbstractTaskProvider(parent),
            filename(filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        qWarning() << "failed to open " << filename;
        return;
    }

    QByteArray data = file.readAll();
    QJsonParseError parseError;
    QJsonDocument json = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        qWarning() << "failed to parse " << filename << " " <<
                    parseError.errorString();
        return;
    }

    QJsonArray jsonTasks = json.array();
    for (int i(0); i < jsonTasks.size(); ++i)
    {
        QJsonValue value = jsonTasks[i];
        if (value.isObject())
        {
            QJsonObject o = value.toObject();
            QString title = o["title"].toString();
            QString startdate = o["start"].toString();
            QString enddate = o["end"].toString();
            LpimTask * newTask = create();
            newTask->setStartTime(
                        QDateTime::fromString(startdate)); // TODO: specify date format
            newTask->setEndTime(
                        QDateTime::fromString(enddate)); // TODO: specify date format
            newTask->setTitle(title);
        }
        else
        {
            qWarning() << "invalid json data: " << value.type()
                     << " object is expected";
        }
    }
}

LpimJsonTaskProvider::~LpimJsonTaskProvider()
{
}

LpimTask *LpimJsonTaskProvider::create()
{
    LPim::LpimTask *task = new LPim::LpimTask;
    tasks.push_back(QSharedPointer<LPim::LpimTask>(task));
    return task;
}

void LpimJsonTaskProvider::fetch(
        const QDateTime &from,
        const QDateTime &to,
        QVector<LpimTaskSharedPtr> &itsTasks)
{
    itsTasks.clear();
    for (int i(0); i < tasks.size(); ++i)
    {
        if (from <= tasks[i]->startTime() &&
                tasks[i]->endTime() < to)
        {
            itsTasks.append(tasks[i]);
        }
    }
}

void LpimJsonTaskProvider::save()
{
    QJsonArray jsonTasks;
    for (int i(0); i < tasks.size(); ++i)
    {
        QJsonObject jsonTask;
        LpimTask &t = *tasks[i];
        jsonTask["title"] = t.title();
        jsonTask["start"] = t.startTime().toString(); // TODO: specify format
        jsonTask["end"] = t.endTime().toString(); // TODO: specify format
        jsonTasks.append(jsonTask);
    }

    QJsonDocument doc(jsonTasks);

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        qWarning() << "failed to open " << filename << " for write";
        return;
    }

    QByteArray jsondata = doc.toJson();
    if (file.write(jsondata) < 0)
    {
        qWarning() << "failed to write " << filename;
        return;
    }

    // TODO: is it changed?
    emit changed();
}



LPIM_END_NAMESPACE
