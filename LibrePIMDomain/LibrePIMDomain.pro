#-------------------------------------------------
#
# Project created by QtCreator 2014-01-02T16:38:47
#
#-------------------------------------------------

QT       += sql xml testlib

QT       -= gui

TARGET = LibrePIMDomain
TEMPLATE = lib

DEFINES += LIBREPIMDOMAIN_LIBRARY

SOURCES += librepimdomain.cpp \
    lpimtask.cpp \
    lpimabstracttaskprovider.cpp \
    lpimsqlitetaskprovider.cpp \
    lpiminmemorytaskprovider.cpp \
    lpimjsontaskprovider.cpp

HEADERS += librepimdomain.h\
        librepimdomain_global.h \
    lpimtask.h \
    lpimabstracttaskprovider.h \
    lpimsqlitetaskprovider.h \
    lpiminmemorytaskprovider.h \
    lpimjsontaskprovider.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

QMAKE_CXXFLAGS += -Werror
QMAKE_CXXFLAGS += -std=c++11
