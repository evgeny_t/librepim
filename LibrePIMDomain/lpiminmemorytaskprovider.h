#ifndef LPIMINMEMORYTASKPROVIDER_H
#define LPIMINMEMORYTASKPROVIDER_H

#include "librepimdomain_global.h"
#include <QObject>
#include "lpimabstracttaskprovider.h"

LPIM_BEGIN_NAMESPACE

class LpimInMemoryTaskProviderImp;

class LIBREPIMDOMAINSHARED_EXPORT LpimInMemoryTaskProvider :
    public LpimAbstractTaskProvider
{
    Q_OBJECT
public:
    explicit LpimInMemoryTaskProvider(QObject *parent = 0);
    virtual ~LpimInMemoryTaskProvider();

    virtual LpimTask* create();
    virtual void fetch(const QDateTime& from, const QDateTime& to,
               QVector<LpimTaskSharedPtr>& tasks);
    virtual void save();
signals:

public slots:
    void onTaskChanged(LpimTask&);
private:
    LpimInMemoryTaskProviderImp *mImpl;
};

LPIM_END_NAMESPACE

#endif // LPIMINMEMORYTASKPROVIDER_H
