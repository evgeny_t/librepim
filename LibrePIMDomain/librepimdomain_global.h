#ifndef LIBREPIMDOMAIN_GLOBAL_H
#define LIBREPIMDOMAIN_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LIBREPIMDOMAIN_LIBRARY)
#  define LIBREPIMDOMAINSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBREPIMDOMAINSHARED_EXPORT Q_DECL_IMPORT
#endif

#define LPIM_NAMESPACE LPim
#define LPIM_BEGIN_NAMESPACE namespace LPIM_NAMESPACE {
#define LPIM_END_NAMESPACE }

#endif // LIBREPIMDOMAIN_GLOBAL_H
