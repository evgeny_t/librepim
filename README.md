# LibrePIM

---  

LibrePIM is intended to be free and open source [personal information manager](http://en.wikipedia.org/wiki/Personal_information_manager).

## Features

* Calendar
* Task list
* Contact list
* TODO

## Contact

[Follow project on Twitter](https://twitter.com/librepim).  
[Follow LibrePIM development Twitter stream](https://twitter.com/librepim_dev).  

## License